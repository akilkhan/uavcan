/*
 * Copyright (c) 2016 UAVCAN Team
 *
 * Distributed under the MIT License, available in the file LICENSE.
 *
 * Author: Michael Sierra <sierramichael.a@gmail.com>
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include "canard.h"
#include "canard_internals.h"

#include "float_spec.h"
#include "int_spec.h"

#include "stm32f10x.h"
#include "stm32f10x_conf.h"

#define UAVCAN_NODE_ID 		20

#define CANARD_AVAILABLE_BLOCKS 32

#define CAN_BAUDRATE	1000	/* 1MBps */
/* #define CAN_BAUDRATE		500 */	/* 500 kBps	*/
/* #define CAN_BAUDRATE		250 */	/* 250 kBps	*/
/* #define CAN_BAUDRATE		125 */	/* 125 kBps	*/
/* #define CAN_BAUDRATE		100 */	/* 100 kBps	*/
/* #define CAN_BAUDRATE		50  */	/* 50 kBps	*/
/* #define CAN_BAUDRATE		20  */	/* 20 kBps	*/
/* #define CAN_BAUDRATE		10 */	/* 10 kBps	*/

CAN_InitTypeDef CAN_InitStructure;
CAN_FilterInitTypeDef CAN_FilterInitStructure;
CanTxMsg TxMessage;

static void Delay(void)
{
	unsigned int i,j;

	for(i=0;i < 10000; i++)
		for(j=0; j<100; j++);
}

typedef struct
{
	uint32_t airspeed;
	int16_t acc;
}TrueAirData;

void uart_init(void)
{
	GPIO_InitTypeDef GPIO_uart;
	USART_InitTypeDef USART_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
/*
	UART_TX -> PD.5, UART_RX -> PD.6
*/
	GPIO_uart.GPIO_Pin = GPIO_Pin_6;
	GPIO_uart.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_uart.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOD,&GPIO_uart);

	GPIO_uart.GPIO_Pin = GPIO_Pin_5;
	GPIO_uart.GPIO_Mode = GPIO_Mode_AF_PP;

	GPIO_Init(GPIOD,&GPIO_uart);

	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART2, &USART_InitStructure);

	USART_Cmd(USART2, ENABLE);
}

void Usart2Put(uint8_t ch)
{
	USART_SendData(USART2, (uint8_t)ch);
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
}

uint8_t Usart2Get(void)
{
	while(USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);
	return (uint8_t)USART_ReceiveData(USART2);
}

int _read(int file, char *ptr, int len)
{
	int n;
	int num = 0;
	switch(file){
	case STDIN_FILENO:
		for(n = 0; n < len; n++){
			char c = Usart2Get();
			*ptr++ = c;
			num++;
		}
		break;
	default:
		return -1;
	}
	return num;
}

int _write(int file, char *ptr, int len)
{
	int n;
	switch(file){
	case STDOUT_FILENO:
		for(n = 0; n < len; n++){
			Usart2Put(*ptr++ & (uint16_t)0x01FF);
		}
		break;
	case STDERR_FILENO:
		for(n = 0; n < len; n++){
			Usart2Put(*ptr++ & (uint16_t)0x01FF);
		}
		break;
	default:
		return -1;
	}
	return len;
}

int can_init()
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

	GPIO_Init(GPIOC,&GPIO_InitStructure);

	/*------ Configure CAN1 and CAN2 IOs -----*/
	/* GPIOD and AFIO Clock enables */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOD, ENABLE);

	/* Configure CAN1 RX Pin */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* Configure CAN1 TX Pin */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* Remap CAN GPIO's */
	GPIO_PinRemapConfig(GPIO_Remap2_CAN1, ENABLE);

	/*CAN Periph clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	/* CAN Register Init */
	CAN_DeInit(CAN1);

	/* CAN struct Init */
	CAN_StructInit(&CAN_InitStructure);

    /* CAN Cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
	CAN_InitStructure.CAN_BS1 = CAN_BS1_2tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;

#if CAN_BAUDRATE == 1000 /* 1 MBps */
	CAN_InitStructure.CAN_Prescaler = 6;
#elif CAN_BAUDRATE == 500 /* 500 KBps */
	CAN_InitStructure.CAN_Prescaler = 12;
#elif CAN_BAUDRATE == 250 /* 250 KBps */
	CAN_InitStructure.CAN_Prescaler = 24;
#elif CAN_BAUDRATE == 125 /* 125 KBps */
	CAN_InitStructure.CAN_Prescaler = 48;
#elif CAN_BAUDRATE == 100 /* 100 KBps */
	CAN_InitStructure.CAN_Prescaler = 60;
#elif CAN_BAUDRATE == 50 /* 50 KBps */
	CAN_InitStructure.CAN_Prescaler = 120;
#elif CAN_BAUDRATE == 20 /* 20 KBps */
	CAN_InitStructure.CAN_Prescaler = 300;
#elif CAN_BAUDRATE == 10 /* 10 KBps */
	CAN_InitStructure.CAN_Prescaler = 600;
#else
#error "Please select first the CAN Baudrate in private defines in main.c"
#endif

	/* Initialize the CAN */
	CAN_Init(CAN1,&CAN_InitStructure);

	/* CAN Filter Init */
	CAN_FilterInitStructure.CAN_FilterNumber = 1;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	return 0;
}

static uint8_t uavcan_node_id;

int can_send(uint32_t extended_can_id, const uint8_t* frame_data, uint8_t frame_data_len)
{
    if (frame_data_len > 8 || frame_data == NULL)
    {
        return -1;
    }

    CanTxMsg TxMessage;
    memset(&TxMessage, 0,sizeof(TxMessage));

    TxMessage.ExtId  = extended_can_id;
    TxMessage.DLC = frame_data_len;
    TxMessage.IDE = 0x4;
    memcpy(TxMessage.Data, frame_data, frame_data_len);

    return CAN_Transmit(CAN1,&TxMessage);
}

uint64_t get_monotonic_usec(void)
{
/*    struct timespec ts;

    if (clock_gettime(CLOCK_MONOTONIC, &ts) != 0)
    {
        return 0;
    }
    return (uint64_t)ts.tv_sec * 1000000ULL + (uint64_t)ts.tv_nsec / 1000UL;*/
	return 0x00000000;
}

// / Arbitrary priority values
static const uint8_t PRIORITY_HIGHEST = 0;
static const uint8_t PRIORITY_HIGH    = 8;
static const uint8_t PRIORITY_MEDIUM  = 16;
static const uint8_t PRIORITY_LOW     = 24;
static const uint8_t PRIORITY_LOWEST  = 31;

// / Defined for the standard data type uavcan.protocol.NodeStatus
enum node_health
{
    HEALTH_OK       = 0,
    HEALTH_WARNING  = 1,
    HEALTH_ERROR    = 2,
    HEALTH_CRITICAL = 3
};

// / Defined for the standard data type uavcan.protocol.NodeStatus
enum node_mode
{
    MODE_OPERATIONAL     = 0,
    MODE_INITIALIZATION  = 1,
    MODE_MAINTENANCE     = 2,
    MODE_SOFTWARE_UPDATE = 3,
    MODE_OFFLINE         = 7
};

// / Standard data type: uavcan.protocol.NodeStatus
int publish_node_status(CanardInstance* ins, enum node_health health, enum node_mode mode,
                        uint16_t vendor_specific_status_code)
{
    static uint64_t startup_timestamp_usec;
    if (startup_timestamp_usec == 0)
    {
        startup_timestamp_usec = get_monotonic_usec();
    }

    uint8_t payload[7];

    // Uptime in seconds
    const uint32_t uptime_sec = (get_monotonic_usec() - startup_timestamp_usec) / 1000000ULL;
    payload[0] = (uptime_sec >> 0)  & 0xFF;
    payload[1] = (uptime_sec >> 8)  & 0xFF;
    payload[2] = (uptime_sec >> 16) & 0xFF;
    payload[3] = (uptime_sec >> 24) & 0xFF;

    // Health and mode
    payload[4] = ((uint8_t)health << 6) | ((uint8_t)mode << 3);

    // Vendor-specific status code
    payload[5] = (vendor_specific_status_code >> 0) & 0xFF;
    payload[6] = (vendor_specific_status_code >> 8) & 0xFF;

    static const uint16_t data_type_id = 341;
    static uint8_t transfer_id;
    uint64_t data_type_signature = 0x8899AABBCCDDEEFF;
    return canardBroadcast(ins, data_type_signature,
                           data_type_id, &transfer_id, PRIORITY_LOW, payload, sizeof(payload));
}

/*
 * Float16 support
 */
uint16_t make_float16(float value)
{
    union fp32
    {
        uint32_t u;
        float f;
    };

    const union fp32 f32infty = { 255U << 23 };
    const union fp32 f16infty = { 31U << 23 };
    const union fp32 magic = { 15U << 23 };
    const uint32_t sign_mask = 0x80000000U;
    const uint32_t round_mask = ~0xFFFU;

    union fp32 in;

    uint16_t out = 0;

    in.f = value;

    uint32_t sign = in.u & sign_mask;
    in.u ^= sign;

    if (in.u >= f32infty.u)
    {
        out = (in.u > f32infty.u) ? 0x7FFFU : 0x7C00U;
    }
    else
    {
        in.u &= round_mask;
        in.f *= magic.f;
        in.u -= round_mask;
        if (in.u > f16infty.u)
        {
            in.u = f16infty.u;
        }
        out = (uint16_t)(in.u >> 13);
    }

    out |= (uint16_t)(sign >> 16);

    return out;
}

// / Standard data type: uavcan.equipment.air_data.TrueAirspeed
int publish_true_airspeed(CanardInstance* ins, TrueAirData data)
{
	uint8_t payload[6];

	memset(&payload, 0, sizeof(payload));

	encodeUint32(&(data.airspeed),payload);
	encodeInt16(&(data.acc),payload+4);

	static const uint16_t data_type_id = 1020;
    static uint8_t transfer_id;
    uint64_t data_type_signature = 0x8899AABBCCDDEEFF;
    return canardBroadcast(ins, data_type_signature,
                           data_type_id, &transfer_id, PRIORITY_MEDIUM, payload, sizeof(payload));
}

// Standard data type: uavcan.equipment.multi
int publish_multi(CanardInstance* ins)
{
    static int len = 20;
    uint8_t payload[len];
    uint8_t i;
    for (i = 0; i<len; i++)
    {
        payload[i] = i + 1;
    }
    static const uint16_t data_type_id = 420;
    static uint8_t transfer_id;
    uint64_t data_type_signature = 0x8899AABBCCDDEEFF;
    return canardBroadcast(ins, data_type_signature,
                           data_type_id, &transfer_id, PRIORITY_HIGH, payload, sizeof(payload));
}

int publish_request(CanardInstance* ins)
{
    static int len = 43;
    uint8_t payload[len];
    uint8_t i;
    for (i = 0; i<len; i++)
    {
        payload[i] = i + 3;
    }
    uint8_t dest_id = 30;
    static const uint16_t data_type_id = 15;
    static uint8_t transfer_id;
    uint64_t data_type_signature = 0x8899AABBCCDDEEFF;
    return canardRequestOrRespond(ins, dest_id, data_type_signature,
                                  data_type_id, &transfer_id, PRIORITY_LOW, CanardRequest, payload, sizeof(payload));
}

int publish_response(CanardInstance* ins)
{
    static int len = 26;
    uint8_t payload[len];
    uint8_t i;
    for (i = 0; i<len; i++)
    {
        payload[i] = i + 3;
    }
    uint8_t dest_id = 30;
    static const uint16_t data_type_id = 15;
    static uint8_t transfer_id;
    uint64_t data_type_signature = 0x8899AABBCCDDEEFF;
    return canardRequestOrRespond(ins, dest_id, data_type_signature,
                                  data_type_id, &transfer_id, PRIORITY_LOW, CanardResponse, payload, sizeof(payload));
}

int compute_true_airspeed(float* out_airspeed, float* out_variance)
{
    *out_airspeed = 1.2345F; // This is a stub.
    *out_variance = 0.0F;    // By convention, zero represents unknown error variance.
    return 0;
}

void printframe(const CanardCANFrame* frame)
{
    printf("%X ", frame->id);
    printf("[%u] ", frame->data_len);
    int i;
    for (i = 0; i < frame->data_len; i++)
    {
        printf(" %02X ", frame->data[i]);
    }
    printf("\r\n");
}

void on_reception(CanardInstance* ins, CanardRxTransfer* transfer)
{
    // printf("\n");
    printf("\r\ntransfer type: \r\n");
    switch (transfer->transfer_type)
    {
    case CanardTransferTypeResponse:
        printf("reponse\n");
        break;
    case CanardTransferTypeRequest:
        printf("request\n");
        break;
    case CanardTransferTypeBroadcast:
        printf("broadcast\n");
        break;
    default:
        break;
    }
    uint8_t payload[transfer->payload_len];
    memset(payload, 0, sizeof payload);
    if (transfer->payload_len > 7)
    {
        CanardBufferBlock* block = transfer->payload_middle;
        int i;
        uint8_t index = 0;
        if (CANARD_RX_PAYLOAD_HEAD_SIZE > 0)
        {
            for (i = 0; i < CANARD_RX_PAYLOAD_HEAD_SIZE; i++, index++)
            {
                payload[i] = transfer->payload_head[i];
            }
        }

        for (i = 0; index < (CANARD_RX_PAYLOAD_HEAD_SIZE + transfer->middle_len); i++, index++)
        {
            payload[index] = block->data[i];
            if (i==CANARD_BUFFER_BLOCK_DATA_SIZE - 1)
            {
                i = -1;
                block = block->next;
            }
        }

        int tail_len = transfer->payload_len - (CANARD_RX_PAYLOAD_HEAD_SIZE + transfer->middle_len);
        for(i = 0; i<(tail_len);i++,index++)
        {
        	payload[index]=transfer->payload_tail[i];
        }
    }
    else
    {
        uint8_t i;
        for (i = 0; i<transfer->payload_len; i++)
        {
            payload[i] = transfer->payload_head[i];
        }
    }

    canardReleaseRxTransferPayload(ins, transfer);
    // do stuff with the data then call canardReleaseRxTransferPayload() if there are blocks (multi-frame transfers)

    int i;
    for (i = 0; i<sizeof(payload); i++)
    {
        printf("%02X ", payload[i]);
    }
    printf("\r\n");
}

bool should_accept(const CanardInstance* ins, uint64_t* out_data_type_signature,
                   uint16_t data_type_id, CanardTransferType transfer_type, uint8_t source_node_id)
{
    *out_data_type_signature = 0x8899AABBCCDDEEFF;
    return true;
}

// returns true with a probability of probability
bool random_drop(double probability)
{
    return rand() <  probability * ((double)RAND_MAX + 1.0);
}

void sendThread(void* canard_instance)
{
    bool drop = false;

    while (1)
    {
        /*
        if ((get_monotonic_usec() - last_node_status) > TIME_TO_SEND_NODE_STATUS)
        {
            const uint16_t vendor_specific_status_code = rand(); // Can be used to report vendor-specific status info
            publish_node_status(canard_instance, health, MODE_OPERATIONAL, vendor_specific_status_code);
            last_node_status = get_monotonic_usec();
        }

        if ((get_monotonic_usec() - last_airspeed) > TIME_TO_SEND_AIRSPEED)
        {
            float airspeed = 0.0F;
            float airspeed_variance = 0.0F;
            const int airspeed_computation_result = compute_true_airspeed(&airspeed, &airspeed_variance);

            if (airspeed_computation_result == 0)
            {
                const int publication_result = publish_true_airspeed(canard_instance, airspeed, airspeed_variance);
                health = (publication_result < 0) ? HEALTH_ERROR : HEALTH_OK;
            }
            else
            {
                health = HEALTH_ERROR;
            }
            last_airspeed = get_monotonic_usec();
        }
        if ((get_monotonic_usec() - last_multi) > TIME_TO_SEND_MULTI)
        {
            publish_multi(canard_instance);
            last_multi = get_monotonic_usec();
        }
        if ((get_monotonic_usec() - last_request) > TIME_TO_SEND_REQUEST)
        {
            publish_request(canard_instance);
            last_request = get_monotonic_usec();
        }
        if ((get_monotonic_usec() - last_clean) > CLEANUP_STALE_TRANSFERS)
        {
            canardCleanupStaleTransfers(canard_instance, get_monotonic_usec());
            last_clean = get_monotonic_usec();
        }
        */
        Delay();

/*
        float airspeed = 0.0F;
        float airspeed_variance = 0.0F;
        const int airspeed_computation_result = compute_true_airspeed(&airspeed, &airspeed_variance);

        if (airspeed_computation_result == 0)
        {
            const int publication_result = publish_true_airspeed(canard_instance, airspeed, airspeed_variance);
            health = (publication_result < 0) ? HEALTH_ERROR : HEALTH_OK;
        }
*/
/*        float airspeed = 0.0F;
        float airspeed_variance = 0.0F;
        const int airspeed_computation_result = compute_true_airspeed(&airspeed,&airspeed_variance);

        publish_true_airspeed(canard_instance, airspeed, airspeed_variance);
*/
      //  publish_multi(canard_instance);

      //  publish_request(canard_instance);

      //   publish_response(canard_instance);

        TrueAirData ad;
        ad.airspeed = 400020;
        ad.acc = 24608;

        publish_true_airspeed(canard_instance, ad);

    	const CanardCANFrame* transmit_frame = canardPeekTxQueue(canard_instance);

    	while (transmit_frame != NULL)
        {
            if (drop)
            {
                printf("dropping\n");
                // printframe(transmit_frame);
                // can_send(transmit_frame->id, transmit_frame->data, transmit_frame->data_len);
            }
            else
            {
                // printf("keeping\n");
                printframe(transmit_frame);
                uint8_t ret = can_send(transmit_frame->id, transmit_frame->data, transmit_frame->data_len);
                if(ret == 0x4)
                {
                	printf("NO mailbox \r\n");
                }
            }
            canardPopTxQueue(canard_instance);
            transmit_frame = canardPeekTxQueue(canard_instance);
        }
    	printf("\r\n");
    }
}

void NVIC_Config()
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

static CanardInstance canard_instance;

int main(void)
{
    /*
     * Node initialization
     */
	SystemInit();
	NVIC_Config();
	uart_init();
	printf("\r\nUART Initialized\r\n");

	if (can_init() < 0)
    {
        printf("Failed to Init CAN");
        return 1;
    }

	/*
    * Main loop
    */

    uavcan_node_id = UAVCAN_NODE_ID; //Should be between 0 and 127

    static CanardPoolAllocatorBlock buffer[CANARD_AVAILABLE_BLOCKS];           // pool blocks
    canardInit(&canard_instance, buffer, sizeof(buffer), on_reception, should_accept);
    canardSetLocalNodeID(&canard_instance, uavcan_node_id);
    printf("\r\nNode Initialized\r\n");

    /*
     * Main loop
     */
 //   CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
    sendThread(&canard_instance);
    while(1);
}

void USB_LP_CAN1_RX0_IRQHandler(void)
{
    CanRxMsg r_frame;
    CanardCANFrame canard_frame;

    //printf("CAN ISR\r\n");
    CAN_Receive(CAN1,CAN_FIFO0,&r_frame);
    memset(&canard_frame, 0, sizeof(canard_frame));

    canard_frame.id = r_frame.ExtId;
    canard_frame.data_len = r_frame.DLC;
    memcpy(canard_frame.data, &r_frame.Data, r_frame.DLC);
    //printframe(&canard_frame);
    canardHandleRxFrame(&canard_instance, &canard_frame, get_monotonic_usec());
}
