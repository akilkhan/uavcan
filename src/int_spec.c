#include "int_spec.h"

/* Helper functions for internal conversion */

void encodeInt8(int8_t* val, uint8_t* bytes)
{
	*bytes = (uint8_t)*val;
}

int8_t decodeInt8(uint8_t* bytes)
{
	return (int8_t)*bytes;
}

void encodeInt8Array(int8_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeInt8((val+i),(bytes+i));
	}
}

void decodeInt8Array(int8_t* val,uint8_t *bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeInt8(bytes++);
	}
}

void encodeUint8(uint8_t* val, uint8_t* bytes)
{
	*bytes = *val;
}

uint8_t decodeUint8(uint8_t* bytes)
{
	return *bytes;
}

void encodeUint8Array(uint8_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeUint8((val+i),(bytes+i));
	}
}

void decodeUint8Array(uint8_t* val, uint8_t *bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeInt8(bytes++);
	}
}

void encodeInt16(int16_t* val, uint8_t* bytes)
{
	*bytes = *val & 0xff;
	*(bytes + 1) = (*val >> 8) & 0xff;
}

int16_t decodeInt16(uint8_t *bytes)
{
	return ( (*(bytes+1) << 8) | *bytes);
}

void encodeInt16Array(int16_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeInt16((val+i),(bytes+i*2));
	}
}

void decodeInt16Array(int16_t* val, uint8_t *bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeInt16(bytes+i*2);
	}
}

void encodeUint16(uint16_t* val, uint8_t* bytes)
{
	*bytes = *val & 0xff;
	*(bytes + 1) = (*val >> 8) & 0xff;
}

uint16_t decodeUint16(uint8_t *bytes)
{
	return ( (*(bytes + 1) << 8) | *bytes);
}

void encodeUint16Array(uint16_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeUint16((val+i),(bytes+i*2));
	}
}

void decodeUint16Array(uint16_t* val, uint8_t *bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeUint16(bytes+i*2);
	}
}

void encodeInt32(int32_t* val, uint8_t* bytes)
{
	*bytes = *val & 0xff;
	*(bytes +  1) = (*val >> 8) & 0xff;
	*(bytes +  2) = (*val >> 16) & 0xff;
	*(bytes +  3) = (*val >> 24) & 0xff;
}

int32_t decodeInt32(uint8_t *bytes)
{
	return ( *bytes | (*(bytes + 1) << 8)| (*(bytes + 2) << 16) | (*(bytes + 3) << 24));
}

void encodeInt32Array(int32_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeInt32((val+i),(bytes+i*4));
	}
}

void decodeInt32Array(int32_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeInt32(bytes+i*4);
	}
}

void encodeUint32(uint32_t* val, uint8_t* bytes)
{
	*bytes = *val & 0xff;
	*(bytes +  1) = (*val >> 8) & 0xff;
	*(bytes +  2) = (*val >> 16) & 0xff;
	*(bytes +  3) = (*val >> 24) & 0xff;
}

uint32_t decodeUint32(uint8_t *bytes)
{
	return ( *bytes | (*(bytes + 1) << 8)| (*(bytes + 2) << 16) | (*(bytes + 3) << 24));
}

void encodeUint32Array(uint32_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeUint32((val+i),(bytes+i*4));
	}
}

void decodeUint32Array(uint32_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeUint32(bytes+i*4);
	}
}

void encodeInt64(int64_t* val, uint8_t* bytes)
{	
	*bytes = *val & 0xff;
	*(bytes +  1) = (*val >> 8) & 0xff;
	*(bytes +  2) = (*val >> 16) & 0xff;
	*(bytes +  3) = (*val >> 24) & 0xff;
	*(bytes +  4) = (*val >> 32) & 0xff;
	*(bytes +  5) = (*val >> 40) & 0xff;
	*(bytes +  6) = (*val >> 48) & 0xff;
	*(bytes +  7) = (*val >> 56) & 0xff;
}
	
int64_t decodeInt64(uint8_t* bytes)
{
	const uint32_t first_dword = *bytes | (*(bytes + 1) << 8)| (*(bytes + 2) << 16) | (*(bytes + 3) << 24);
	const uint64_t second_dword = *(bytes + 4) | (*(bytes + 5) << 8)| (*(bytes + 6) << 16) | (*(bytes + 7) << 24);
	
	return (first_dword | (second_dword << 32));
}

void encodeInt64Array(int64_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeInt64((val+i),(bytes+i*8));
	}
}

void decodeInt64Array(int64_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeInt64(bytes+i*8);
	}
}

void encodeUint64(uint64_t* val, uint8_t* bytes)
{
	*bytes = *val & 0xff;
	*(bytes +  1) = (*val >> 8) & 0xff;
	*(bytes +  2) = (*val >> 16) & 0xff;
	*(bytes +  3) = (*val >> 24) & 0xff;
	*(bytes +  4) = (*val >> 32) & 0xff;
	*(bytes +  5) = (*val >> 40) & 0xff;
	*(bytes +  6) = (*val >> 48) & 0xff;
	*(bytes +  7) = (*val >> 56) & 0xff;
}

uint64_t decodeUint64(uint8_t* bytes)
{
	const uint32_t first_dword = *bytes | (*(bytes + 1) << 8)| (*(bytes + 2) << 16) | (*(bytes + 3) << 24);
	const uint64_t second_dword = *(bytes + 4) | (*(bytes + 5) << 8)| (*(bytes + 6) << 16) | (*(bytes + 7) << 24);
	
	return (first_dword | (second_dword << 32));
}

void encodeUint64Array(uint64_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeUint64((val+i),(bytes+i*8));
	}
}

void decodeUint64Array(uint64_t* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeUint64(bytes+i*8);
	}
}
