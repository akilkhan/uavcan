#include <stdio.h>
#include "prime_vectors.h"

extern unsigned long _stext;
extern unsigned long _etext;
extern unsigned long _sdata;
extern unsigned long _edata;
extern unsigned long _sbss;
extern unsigned long _ebss;
extern unsigned long _estack;

extern int main(void);

void ResetHandler(void);

extern void (* const gVectors[])(void);
__attribute__ ((section(".vectors"), used))
void (* const gVectors[])(void) =
{
	(void (*)(void))((unsigned long)&_estack),
	ResetHandler,
	NMI_Handler,
	HardFault_Handler,
	MemManage_Handler,
	BusFault_Handler,
	UsageFault_Handler,
	0,0,0,0,
	SVC_Handler,
	DebugMon_Handler,
	0,
	PendSV_Handler,
	SysTick_Handler,
	WWDG_IRQHandler,
	PVD_IRQHandler,
        TAMPER_IRQHandler,
        RTC_IRQHandler,
        FLASH_IRQHandler,
        RCC_IRQHandler,
        EXTI0_IRQHandler,
        EXTI1_IRQHandler,
        EXTI2_IRQHandler,
        EXTI3_IRQHandler,
        EXTI4_IRQHandler,
        DMA1_Channel1_IRQHandler,
        DMA1_Channel2_IRQHandler,
        DMA1_Channel3_IRQHandler,
        DMA1_Channel4_IRQHandler,
        DMA1_Channel5_IRQHandler,
        DMA1_Channel6_IRQHandler,
        DMA1_Channel7_IRQHandler,
        ADC1_2_IRQHandler,
        USB_HP_CAN1_TX_IRQHandler,
        USB_LP_CAN1_RX0_IRQHandler,
        CAN1_RX1_IRQHandler,
        CAN1_SCE_IRQHandler,
        EXTI9_5_IRQHandler,
        TIM1_BRK_IRQHandler,
        TIM1_UP_IRQHandler,
        TIM1_TRG_COM_IRQHandler,
        TIM1_CC_IRQHandler,
        TIM2_IRQHandler,
        TIM3_IRQHandler,
        TIM4_IRQHandler,
        I2C1_EV_IRQHandler,
	I2C1_ER_IRQHandler,
	I2C2_EV_IRQHandler,	
	I2C2_ER_IRQHandler,
	SPI1_IRQHandler,
	SPI2_IRQHandler,
	USART1_IRQHandler,	
	USART2_IRQHandler,
	USART3_IRQHandler,
	EXTI15_10_IRQHandler,
	RTCAlarm_IRQHandler,
	USBWakeUp_IRQHandler,
	TIM8_BRK_IRQHandler,
	TIM8_UP_IRQHandler,
	TIM8_TRG_COM_IRQHandler,	
	TIM8_CC_IRQHandler,
	ADC3_IRQHandler,
	FSMC_IRQHandler,	
	SDIO_IRQHandler,
	TIM5_IRQHandler,
	SPI3_IRQHandler,
	UART4_IRQHandler,
	UART5_IRQHandler,
	TIM6_IRQHandler,
	TIM7_IRQHandler,
	DMA2_Channel1_IRQHandler,
	DMA2_Channel2_IRQHandler,
	DMA2_Channel3_IRQHandler,
	DMA2_Channel4_5_IRQHandler
}; /* gVectors */

/***************************************************************************/
/*  ResetHandler                                                           */
/*                                                                         */
/*  This function is used for the C runtime initialisation,                */
/*  for handling the .data and .bss segments.                              */
/***************************************************************************/
void __attribute__((noreturn)) ResetHandler (void)
{
   uint32_t *pSrc;
   uint32_t *pDest;


   /*
    * Set the "Vector Table Offset Register". From the ARM
    * documentation, we got the following information:
    *
    * Use the Vector Table Offset Register to determine:
    *  - if the vector table is in RAM or code memory
    *  - the vector table offset.
    */
   *((uint32_t*)0xE000ED08) = (uint32_t)&_stext;

   /*
    * Copy the initialized data of the ".data" segment
    * from the flash to the are in the ram.
    */
   pSrc  = &_etext;
   pDest = &_sdata;
   while(pDest < &_edata)
   {
      *pDest++ = *pSrc++;
   }

   /*
    * Clear the ".bss" segment.
    */
   pDest = &_sbss;
   while(pDest < &_ebss)
   {
      *pDest++ = 0;
   }

   /*
    * And now the main function can be called.
    * Scotty, energie...
    */
   main();

   /*
    * In case there are problems with the
    * "warp drive", stop here.
    */
   while(1) {};

} /* ResetHandler */


/*
 * And here are the weak interrupt handlers.
 */
void __attribute__((weak, noreturn)) NMI_Handler (void){while(1);}
void __attribute__((weak, noreturn)) HardFault_Handler (void){while(1);}
void __attribute__((weak, noreturn)) MemManage_Handler (void){while(1);}
void __attribute__((weak, noreturn)) BusFault_Handler (void){while(1);}
void __attribute__((weak, noreturn)) UsageFault_Handler (void){while(1);}
void __attribute__((weak, noreturn)) SVC_Handler (void){while(1);}
void __attribute__((weak, noreturn)) DebugMon_Handler (void){while(1);}
void __attribute__((weak, noreturn)) PendSV_Handler (void){while(1);}
void __attribute__((weak, noreturn)) SysTick_Handler (void){while(1);}
void __attribute__((weak, noreturn)) WWDG_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) PVD_IRQHandler  (void){while(1);}
void __attribute__((weak, noreturn)) TAMPER_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) RTC_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) FLASH_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) RCC_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI0_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI1_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI4_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel1_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel4_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel5_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel6_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA1_Channel7_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) ADC1_2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) USB_HP_CAN1_TX_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) USB_LP_CAN1_RX0_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) CAN1_RX1_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) CAN1_SCE_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI9_5_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM1_BRK_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM1_UP_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM1_TRG_COM_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM1_CC_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM4_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) I2C1_EV_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) I2C1_ER_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) I2C2_EV_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) I2C2_ER_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) SPI1_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) SPI2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) USART1_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) USART2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) USART3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) EXTI15_10_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) RTCAlarm_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) USBWakeUp_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM8_BRK_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM8_UP_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM8_TRG_COM_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM8_CC_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) ADC3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) FSMC_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) SDIO_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM5_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) SPI3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) UART4_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) UART5_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM6_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) TIM7_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA2_Channel1_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA2_Channel2_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA2_Channel3_IRQHandler (void){while(1);}
void __attribute__((weak, noreturn)) DMA2_Channel4_5_IRQHandler (void){while(1);}

/*** EOF ***/


