#include "float_spec.h"

/* Helper functions for internal conversion */

static uint16_t float16ToU16(__fp16 *value)
{
	const fp32 f32infty = {255 << 23};
	const fp32 f16infty = {31U << 23};
	const fp32 magic = {15U << 23};
	const uint32_t sign_mask = 0x80000000U;
	const uint32_t round_mask = ~0xFFFU;

	fp32 in;
	uint16_t out = 0;

	in.f = *value;

	uint32_t sign = in.u & sign_mask;
	in.u ^= sign;

	if(in.u >= f32infty.u)
	{
		out = (in.u > f32infty.u) ? 0x7FFFU : 0x7C00U;
	}
	else
	{
		in.u &= round_mask;
		in.f *= magic.f;
		in.u -= round_mask;
		if(in.u > f16infty.u)
		{
			in.u = f16infty.u;
		}
		out = (uint16_t)(in.u >> 13);
	}
	out |= (uint16_t)(sign >> 16);

	return out;
}

static float u16ToFloat16(uint16_t value)
{
	const fp32 magic = { (254U - 15U) << 23 };
	const fp32 was_infnan = { (127U + 16U) << 23};
	fp32 out;

	out.u = (value & 0x7FFFU) << 13;
	out.f *= magic.f;
	if(out.f >= was_infnan.f)
	{
		out.u |= 255U << 23;
	}
	out.u |= (value & 0x8000U) << 16;

	return out.f;
}

static uint32_t float32ToU32(float *value)
{
	union
	{
		uint32_t i;
		float f;
	}u;

	u.f = *value;
	return u.i;
}

static float u32ToFloat32(uint32_t value)
{
	union
	{
		float f;
		uint32_t i;
	}u;

	u.i = value;
	return u.f;
}

static uint64_t float64ToU64(double *value)
{
	union
	{
		double d;
		uint64_t i;
	}u;

	u.d = *value;
	return u.i;
}

static double u64ToFloat64(uint64_t value)
{
	union
	{
		double d;
		uint64_t i;
	}u;

	u.i = value;
	return u.d;
}

void encodeFloat16(__fp16 *val, uint8_t* bytes)
{
	const uint16_t res = float16ToU16(val);
	*bytes = res & 0xff;
	*(bytes + 1) = (res >> 8) & 0xff;
}

float decodeFloat16(uint8_t* bytes)
{
	return u16ToFloat16((*(bytes+1) << 8) | *(bytes));
}

void encodeFloat16Array(__fp16 *val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeFloat16((val+i),(bytes+i*2));
	}
}

void decodeFloat16Array(__fp16 *val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeFloat16(bytes + i*2);
	}
}

void encodeFloat32(float* val, uint8_t* bytes)
{
	const uint32_t res = float32ToU32(val);

	*bytes = res & 0xff;
	*(bytes + 1) = (res >> 8) & 0xff;
	*(bytes + 2) = (res >> 16) & 0xff;
	*(bytes + 3) = (res >> 24) & 0xff;
}

float decodeFloat32(uint8_t* bytes)
{
	return u32ToFloat32( *bytes | (*(bytes + 1) << 8)| (*(bytes + 2) << 16) | (*(bytes + 3) << 24));
}

void encodeFloat32Array(float *val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeFloat32((val+i),(bytes + i*8));
	}
}

void decodeFloat32Array(float *val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeFloat32(bytes + i*8);
	}
}

void encodeFloat64(double* val, uint8_t* bytes)
{
	const uint64_t res = float64ToU64(val);

	*bytes = res & 0xff;
	*(bytes + 1) = (res >> 8) & 0xff;
	*(bytes + 2) = (res >> 16) & 0xff;
	*(bytes + 3) = (res >> 24) & 0xff;
	*(bytes + 4) = (res >> 32) & 0xff;
	*(bytes + 5) = (res >> 40) & 0xff;
	*(bytes + 6) = (res >> 48) & 0xff;
	*(bytes + 7) = (res >> 56) & 0xff;
}

double decodeFloat64(uint8_t* bytes)
{
	const uint32_t first_dword = *bytes | (*(bytes + 1) << 8)| (*(bytes + 2) << 16) | (*(bytes + 3) << 24);
	const uint64_t second_dword = *(bytes + 4) | (*(bytes + 5) << 8)| (*(bytes + 6) << 16) | (*(bytes + 7) << 24);

	const uint64_t res = first_dword | (second_dword << 32);
	return u64ToFloat64(res);
}

void encodeFloat64Array(double *val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeFloat64((val+i),(bytes + i*8));
	}
}

void decodeFloat64Array(double *val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeFloat64(bytes + i*8);
	}
}
