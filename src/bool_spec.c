#include "bool_spec.h"

void encodeBool(bool *val, uint8_t* bytes)
{
	*bytes = (*val << 7);
}

bool decodeBool(uint8_t* bytes)
{
	if( *bytes & 0x80)
		return true;
	else
		return false;
}

void encodeBoolArray(bool* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		encodeBool((val+i),(bytes+i));
	}

}

void decodeBoolArray(bool* val, uint8_t* bytes, uint32_t len)
{
	uint32_t i;
	for(i = 0; i < len; i++)
	{
		val[i] = decodeBool(bytes+i);
	}
}
