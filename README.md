# Libcanard

Minimal implementation of the UAVCAN protocol stack in C for resource constrained applications. Tested and working on STM32F1. Still need proper restructuring.

## Library development

The library design document can be found in [DESIGN.md](DESIGN.md)

