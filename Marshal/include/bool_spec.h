#include <stdint.h>
#include <stdbool.h>

void encodeBool(bool *val, uint8_t* bytes);
bool decodeBool(uint8_t* bytes);
void encodeBoolArray(bool* val, uint8_t* bytes, uint32_t len);
void decodeBoolArray(bool* val, uint8_t* bytes, uint32_t len);

