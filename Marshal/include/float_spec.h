#include <stdio.h>

#pragma once

typedef union
{
	uint32_t u;
	float f;
}fp32;

/** These functions should be called from the generated library message send/receive functions
 * for serialization/deserialization of the message data structure*/

void encodeFloat16(__fp16 *val, uint8_t* bytes);
float decodeFloat16(uint8_t* bytes);
void encodeFloat16Array(__fp16 *val, uint8_t* bytes, uint32_t len);
void decodeFloat16Array(__fp16 *val, uint8_t* bytes, uint32_t len);

void encodeFloat32(float* val, uint8_t* bytes);
float decodeFloat32(uint8_t* bytes);
void encodeFloat32Array(float *val, uint8_t* bytes, uint32_t len);
void decodeFloat32Array(float *val, uint8_t* bytes, uint32_t len);

void encodeFloat64(double* val, uint8_t* bytes);
double decodeFloat64(uint8_t* bytes);
void encodeFloat64Array(double *val, uint8_t* bytes, uint32_t len);
void decodeFloat64Array(double *val, uint8_t* bytes, uint32_t len);
