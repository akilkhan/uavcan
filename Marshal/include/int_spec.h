#include <stdio.h>

#pragma once

/* Signed 8-bit Integer */
void encodeInt8(int8_t* val, uint8_t* bytes);
int8_t decodeInt8(uint8_t* bytes);
void encodeInt8Array(int8_t* val, uint8_t* bytes, uint32_t len);
void decodeInt8Array(int8_t* val, uint8_t* bytes, uint32_t len);

/* Unsigned 8-bit Integer */
void encodeUint8(uint8_t* val, uint8_t* bytes);
uint8_t decodeUint8(uint8_t* bytes);
void encodeUint8Array(uint8_t* val, uint8_t* bytes, uint32_t len);
void decodeUint8Array(uint8_t* val, uint8_t* bytes, uint32_t len);

/* Signed 16-bit Integer */
void encodeInt16(int16_t* val, uint8_t* bytes);
int16_t decodeInt16(uint8_t *bytes);
void encodeInt16Array(int16_t* val, uint8_t* bytes, uint32_t len);
void decodeInt16Array(int16_t* val, uint8_t* bytes, uint32_t len);

/* Unsigned 16-bit Integer */
void encodeUint16(uint16_t* val, uint8_t* bytes);
uint16_t decodeUint16(uint8_t *bytes);
void encodeUint16Array(uint16_t* val, uint8_t* bytes, uint32_t len);
void decodeUint16Array(uint16_t* val, uint8_t* bytes, uint32_t len);

/* Signed 32-bit Integer */
void encodeInt32(int32_t* val, uint8_t* bytes);
int32_t decodeInt32(uint8_t *bytes);
void encodeInt32Array(int32_t* val, uint8_t* bytes, uint32_t len);
void decodeInt32Array(int32_t* val, uint8_t* bytes, uint32_t len);

/* Unsigned 32-bit Integer */
void encodeUint32(uint32_t* val, uint8_t* bytes);
uint32_t decodeUint32(uint8_t *bytes);
void encodeUint32Array(uint32_t* val, uint8_t* bytes, uint32_t len);
void decodeUint32Array(uint32_t* val, uint8_t* bytes, uint32_t len);

/* Signed 64-bit Integer */
void encodeInt64(int64_t* val, uint8_t* bytes);
int64_t decodeInt64(uint8_t* bytes);
void encodeInt64Array(int64_t* val, uint8_t* bytes, uint32_t len);
void decodeInt64Array(int64_t* val, uint8_t* bytes, uint32_t len);

/* Unsigned 64-bit Integer */
void encodeUint64(uint64_t* val, uint8_t* bytes);
uint64_t decodeUint64(uint8_t* bytes);
void encodeUint64Array(uint64_t* val, uint8_t* bytes, uint32_t len);
void decodeUint64Array(uint64_t* val, uint8_t* bytes, uint32_t len);

